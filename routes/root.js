module.exports = (router) => {
    router.get('/', ctx => {
        ctx.status = 200
        ctx.body = {
            metadata: {
                resources: [
                    {
                        title: 'conversations',
                        href: '/api/v1/conversations'
                    }
                ]
            }
        }
    })
}
