const handler = require('../controllers/conversations')

module.exports = (router) => {
    const ENDPOINT = '/conversations'

    router
        .get(ENDPOINT, ctx => handler.listConversations(ctx))
        .get(`${ENDPOINT}/:conversation_id`, ctx => handler.conversationDetails(ctx))
        .post(ENDPOINT, ctx => handler.createConversation(ctx))
        .post(`${ENDPOINT}/:conversation_id`, ctx => handler.sendMessage(ctx))
}
