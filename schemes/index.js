const Joi = require('joi')

exports.createConversationSchema = Joi.array().length(2)
    .items(Joi.string().min(4).lowercase().required())

exports.listConversationSchema = Joi.object().keys({
    offset: Joi.number().integer().min(0).required(),
    limit: Joi.number().integer().min(1).required(),
    user_id: Joi.string().min(4).lowercase().required()
})

exports.conversationDetailsSchema = Joi.object().keys({
    offset: Joi.number().integer().min(0).required(),
    limit: Joi.number().integer().min(1).required(),
    conversation_id: Joi.string().min(4).lowercase().required()
})

exports.sendMessageSchema = Joi.object().keys({
    from: Joi.string().min(4).lowercase().required(),
    messageBody: Joi.string().lowercase().required(),
    conversation_id: Joi.string().min(4).lowercase().required()
})
