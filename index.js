const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const helmet = require('koa-helmet');

const generalErrorHandler = require('./lib/middleware/error_handler');
const setRoutes = require('./routes');
const { bootstrapDB } = require('./lib/helpers/index');
// server info
const PORT = parseInt(process.env.PORT) || 3100;

const app = new Koa();
if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'test') {
  // load .envs
  require('dotenv').config();
  app.use(require('koa-logger')());
}
// connect to DB
const { Client } = require('@elastic/elasticsearch');
setTimeout(() => {
  const db = new Client({ node: process.env.ES_URL });
  bootstrapDB(db);
  app.context.db = db;
  app.use(cors());
  app.use(helmet());
  app.use(bodyParser());
  // general exception handler
  app.use(generalErrorHandler);
  // setup routes
  setRoutes(app);
  // bootstrap the app
  const server = app.listen(PORT, () => {
    console.log(`app is running on port ${PORT}`);
  });

  module.exports = {
    server,
  };
}, 30000);
