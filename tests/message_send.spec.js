const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const {conversations: conversationSeeds} = require('../lib/seeds/index');
const path = 'conversations';
chai.use(chaiHttp);

describe('Conversations APIs - MessageSend', () => {
  after(() => {
    server.close();
  });
  describe('When a user tries to send a message to a conversation', () => {
    it('It should succeed and send back the created message', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}/${conversationSeeds[0].id}`)
          .set('Content-Type', 'application/json')
          .send({
            from: conversationSeeds[0].parties[0],
            body: 'This is a new message',
          })
          .end((err, res) => {
            expect(res.body).to.be.an('object');
            expect(res.body).to.have.property('data');
            expect(res.body).to.have.nested.property('data.conversation_id');
            expect(res.body).nested.property('data.conversation_id')
                .eq(conversationSeeds[0].id);
            done();
          });
    });
  });
  describe(`When a user tries to send a message to a conversation 
    with an invalid ID`, () => {
    it('It should throw a 400 Bad Request error for incorrect ID', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}/001`)
          .set('Content-Type', 'application/json')
          .send({
            from: conversationSeeds[0].parties[0],
            body: 'This is a new test',
          })
          .end((err, res) => {
            expect(res).to.have.status(400);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
  describe(`When a user tries to send a message to a conversation 
    which does not exist in the system`, () => {
    it('It should throw a 404 not found error', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}/${uuidv1()}`)
          .set('Content-Type', 'application/json')
          .send({
            from: conversationSeeds[0].parties[0],
            body: 'This is a new test',
          })
          .end((err, res) => {
            expect(res).to.have.status(404);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
});
