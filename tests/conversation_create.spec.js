const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const path = 'conversations';
chai.use(chaiHttp);

describe('Conversations APIs - ConversationCreate', () => {
  after(() => {
    server.close();
  });
  describe(`When a user tries to create a new conversation 
  with IDs as parties\' identifiers`,
  () => {
    it('It should succeed and send back created conversation', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}`)
          .set('Content-Type', 'application/json')
          .send({
            parties: [
              '459dc01c-685a-450a-862d-ffe120fd6f17',
              '2aab9b60-0b7f-4f8d-8133-a18b526e22c1',
            ],
          })
          .end((err, res) => {
            expect(res.body).to.be.an('object');
            expect(res.body).to.have.property('data');
            expect(res.body).to.have.nested.property('data.id');
            done();
          });
    });
  });
  describe(`When a user tries to create a new conversation 
  with invalid IDs as parties\' identifiers`,
  () => {
    it('It should throw a 400 Bad Request error for incorrect ID(s)',
        (done) => {
          chai
              .request(server)
              .post(`${BASE_URL}/${path}`)
              .set('Content-Type', 'application/json')
              .send({
                parties: [
                  '459dc01c-685a-450a-862d-ffe120fd6f17',
                  '001',
                ],
              })
              .end((err, res) => {
                expect(res).to.have.status(400);
                expect(res.body).to.be.an('object');
                done();
              });
        });
  });
  describe(`When a user tries to create a new conversation 
  with IDs which are not users of the system`, () => {
    it('It should throw a 404 not found error for missing party', (done) => {
      chai
          .request(server)
          .post(`${BASE_URL}/${path}`)
          .set('Content-Type', 'application/json')
          .send({
            parties: [
              '459dc01c-685a-450a-862d-ffe120fd6f17',
              uuidv1(),
            ],
          })
          .end((err, res) => {
            expect(res).to.have.status(404);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
});
