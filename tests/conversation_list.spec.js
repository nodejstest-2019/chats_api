const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const path = 'conversations';
chai.use(chaiHttp);

describe('Conversations APIs - ConversationList', () => {
  after(() => {
    server.close();
  });
  describe('When someone tries to get a list of conversations based on user ID',
      () => {
        it('It should succeed and send back a pagination of them', (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}`)
              .query({
                user_id: '459dc01c-685a-450a-862d-ffe120fd6f17',
                offset: 0,
                limit: 10,
              })
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body).nested.property('data').to.be.an('array');
                done();
              });
        });
      });
  describe(`When someone tries to get a list of conversations 
  of a user with invalid user ID`,
  () => {
    it('It should fail and send back 400 with apprpriate message',
        (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}`)
              .query({
                user_id: '001',
                offset: 0,
                limit: 10,
              })
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(400);
                expect(res.body).to.be.an('object');
                done();
              });
        });
  });
  describe(`When someone tries to get a list of conversations 
    of a user that does not exist in the system`,
  () => {
    it('It should fail and send back 404 with apprpriate message', (done) => {
      chai
          .request(server)
          .get(`${BASE_URL}/${path}`)
          .query({
            user_id: uuidv1(),
            offset: 0,
            limit: 10,
          })
          .set('Content-Type', 'application/json')
          .end((err, res) => {
            expect(res).to.have.status(404);
            expect(res.body).to.be.an('object');
            done();
          });
    });
  });
});
