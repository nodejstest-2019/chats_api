const chai = require('chai');
const chaiHttp = require('chai-http');
const uuidv1 = require('uuid/v1');
const {server} = require('../index');
const expect = chai.expect;
const {BASE_URL} = require('../constants/index');
const path = 'conversations';
const {conversations: conversationSeeds} = require('../lib/seeds/index');
chai.use(chaiHttp);

describe('Conversations APIs - ConversationDetail', () => {
  after(() => {
    server.close();
  });
  describe(`When someone tries to get the details 
  of a conversation based on its ID`,
  () => {
    it('It should succeed and send back a complete conversation',
        (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}/${conversationSeeds[0].id}`)
              .query({
                offset: 0,
                limit: 10,
              })
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('object');
                expect(res.body).to.have.property('data');
                expect(res.body).nested.property('data.id')
                    .eq(conversationSeeds[0].id);
                done();
              });
        });
  });
  describe(`When someone tries to get the details 
  of a conversation with an invalid ID`,
  () => {
    it('It should fail and send back 400 with apprpriate message',
        (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}/001`)
              .query({
                offset: 0,
                limit: 10,
              })
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(400);
                expect(res.body).to.be.an('object');
                done();
              });
        });
  });
  describe(`When someone tries to get the details of a conversation 
    which does not exist in the system`,
  () => {
    it('It should fail and send back 404 with apprpriate message',
        (done) => {
          chai
              .request(server)
              .get(`${BASE_URL}/${path}/${uuidv1()}`)
              .query({
                offset: 0,
                limit: 10,
              })
              .set('Content-Type', 'application/json')
              .end((err, res) => {
                expect(res).to.have.status(404);
                expect(res.body).to.be.an('object');
                done();
              });
        });
  });
});
