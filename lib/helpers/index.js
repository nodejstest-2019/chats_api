const { conversations, messages } = require('../seeds/index')

// DB utilities
exports.bootstrapDB = db => setInitialDBState(db, [
    {
        name: process.env.CONVERSATIONS_INDEX,
        body: {
            mappings: {
                properties: {
                    parties: { type: 'keyword' },
                    required_matches: { type: 'long' },
                    created_at: { type: 'date' },
                    updated_at: { type: 'date' }
                }
            }
        }
    },
    {
        name: process.env.MESSAGES_INDEX,
        body: {
            mappings: {
                properties: {
                    conversation_id: { type: 'keyword' },
                    from: { type: 'text' },
                    body: { type: 'text' },
                    created_at: { type: 'date' },
                    updated_at: { type: 'date' }
                }
            }
        }
    }
])

const setInitialDBState = async (db, indices) => {
    for (let i = 0; i < indices.length; i++) {
        // check if index exists
        let result = await db.indices.exists({
            index: indices[i].name
        })
        // if not, create the index
        if (result.statusCode === 404 && !result.body) {
            result = await createIndex(db, indices[i].name, indices[i].body)
            // halt the app if index creation fails
            if (result.statusCode !== 200 || !result.body.acknowledged) process.exit(1)
        }
    }
    // seed DB 
    // TODO: replace with bulk API
    conversations.forEach(async conversation => {
        await db.index({
            index: process.env.CONVERSATIONS_INDEX,
            id: conversation.id,
            body: {
                parties: conversation.parties,
                created_at: conversation.create_at,
                updated_at: conversation.updated_at
            }
        })
    })
    await db.indices.refresh({ index: process.env.CONVERSATIONS_INDEX })
    messages.forEach(async message => {
        await db.index({
            index: process.env.MESSAGES_INDEX,
            id: message.id,
            body: {
                conversation_id: message.conversation_id,
                from: message.from,
                body: message.body,
                created_at: message.created_at,
                updated_at: message.updated_at
            }
        })
    })
    await db.indices.refresh({ index: process.env.MESSAGES_INDEX })
}

const createIndex = (db, index, body) => {
    return db.indices.create({
        index: index,
        body: body
    })
}
