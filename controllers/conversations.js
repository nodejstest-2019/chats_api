const Joi = require('joi')
const request = require('request-promise-native');
const {
    createConversationSchema,
    listConversationSchema,
    conversationDetailsSchema,
    sendMessageSchema
} = require('../schemes/index')

exports.createConversation = async ctx => {
    try {
        const { parties } = ctx.request.body
        if (parties) {
            const validationResult = Joi.validate(parties, createConversationSchema)
            if (validationResult.error) {
                const error = {
                    status: 400,
                    title: 'Validation failed',
                    message: validationResult.error.message
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            } else {
                // check if it already exists
                const { body } = await ctx.db.search({
                    index: process.env.CONVERSATIONS_INDEX,
                    body: {
                        query: {
                            terms_set: {
                                parties: {
                                    terms: [parties[0], parties[1]],
                                    minimum_should_match_field: 'required_matches'
                                }
                            }
                        }
                    }
                })
                if (body && body.hits.hits.length > 0) {
                    const existingConversation = {
                        id: body.hits.hits[0]['_id'],
                        parties: [body.hits.hits[0]['_source'].parties[0], body.hits.hits[0]['_source'].parties[1]],
                        created_at: body.hits.hits[0]['_source'].created_at,
                        updated_at: body.hits.hits[0]['_source'].updated_at
                    }
                    ctx.status = 200
                    ctx.body = {
                        data: existingConversation
                    }
                } else {
                    try {
                        // check if parties exist
                        const response1 = await request({
                            uri: `${process.env.USERS_SERVICE}/api/v1/users/${parties[0]}`,
                            method: 'GET',
                            json: true
                        })
                        const response2 = await request({
                            uri: `${process.env.USERS_SERVICE}/api/v1/users/${parties[1]}`,
                            method: 'GET',
                            json: true
                        })
                        if (response1.data && response2.data) {
                            const now = new Date().toISOString()
                            const { body } = await ctx.db.index({
                                index: process.env.CONVERSATIONS_INDEX,
                                body: {
                                    parties: [parties[0], parties[1]],
                                    required_matches: 2,
                                    created_at: now,
                                    updated_at: now
                                }
                            })
                            if (body && body.result === 'created') {
                                await ctx.db.indices.refresh({ index: process.env.CONVERSATIONS_INDEX })
                                const { body } = await ctx.db.search({
                                    index: process.env.CONVERSATIONS_INDEX,
                                    body: {
                                        query: {
                                            terms_set: {
                                                parties: {
                                                    terms: [parties[0], parties[1]],
                                                    minimum_should_match_field: 'required_matches'
                                                }
                                            }
                                        }
                                    }
                                })
                                const newConversation = {
                                    id: body.hits.hits[0]['_id'],
                                    parties: [body.hits.hits[0]['_source'].parties[0], body.hits.hits[0]['_source'].parties[1]],
                                    created_at: body.hits.hits[0]['_source'].created_at,
                                    updated_at: body.hits.hits[0]['_source'].updated_at
                                }
                                ctx.status = 201
                                ctx.body = {
                                    data: newConversation
                                }
                            } else {
                                const error = {
                                    status: 500,
                                    title: 'Internal server error',
                                    message: 'Can\'t index at this time'
                                }
                                ctx.status = error.status
                                ctx.body = {
                                    error: error
                                }
                            }
                        } else {
                            const error = {
                                status: 404,
                                title: 'Not found',
                                message: 'One or both of the parties not found'
                            }
                            ctx.status = error.status
                            ctx.body = {
                                error: error
                            }
                        }
                    } catch (err) {
                        ctx.status = err.statusCode;
                        ctx.body = err.response.body;
                    }
                }
            }
        } else {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: 'No parties specified'
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

exports.listConversations = async ctx => {
    try {
        const validationResult = Joi.validate(ctx.query, listConversationSchema)
        if (validationResult.error) {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: validationResult.error.message
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        } else {
            const { offset, limit, user_id } = ctx.query
            const { body } = await ctx.db.search({
                index: process.env.CONVERSATIONS_INDEX,
                body: {
                    from: offset,
                    size: limit,
                    query: {
                        term: {
                            parties: user_id
                        }
                    }
                }
            })
            if (body && body.hits.hits.length > 0) {
                let data = []
                for (const conversation of body.hits.hits) {
                    let item = {}
                    item.id = conversation['_id']
                    item.parties = conversation['_source'].parties
                    item.created_at = conversation['_source'].created_at
                    item.updated_at = conversation['_source'].updated_at
                    const { body: messageBody } = await ctx.db.search({
                        index: process.env.MESSAGES_INDEX,
                        body: {
                            query: {
                                term: { conversation_id: item.id }
                            },
                            sort: [
                                { created_at: { order: 'desc' } }
                            ],
                            size: 1,
                        }
                    })
                    if (messageBody && messageBody.hits.hits.length > 0) {
                        item.lastMessage = {
                            id: messageBody.hits.hits[0]['_id'],
                            conversation_id: item.id,
                            from: messageBody.hits.hits[0]['_source'].from,
                            body: messageBody.hits.hits[0]['_source'].body,
                            created_at: messageBody.hits.hits[0]['_source'].created_at,
                            updated_at: messageBody.hits.hits[0]['_source'].updated_at
                        }
                    }
                    data.push(item)
                }
                ctx.status = 200
                ctx.body = {
                    data: data
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: `User with ID ${user_id} does not have any conversations yet`
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

exports.conversationDetails = async ctx => {
    try {
        const { offset, limit } = ctx.query
        const { conversation_id } = ctx.params
        const validationResult = Joi.validate(
            { conversation_id: conversation_id, ...ctx.query },
            conversationDetailsSchema
        )
        if (validationResult.error) {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: validationResult.error.message
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        } else {
            const { body } = await ctx.db.search({
                index: process.env.CONVERSATIONS_INDEX,
                body: {
                    query: {
                        ids: {
                            values: [conversation_id]
                        }
                    }
                }
            })
            if (body.hits && body.hits.hits.length > 0) {
                const { body: messageBody } = await ctx.db.search({
                    index: process.env.MESSAGES_INDEX,
                    body: {
                        query: {
                            term: { conversation_id: conversation_id }
                        },
                        from: offset,
                        size: limit,
                        sort: [{ updated_at: 'desc' }, { created_at: 'desc' }]
                    }
                })
                const response = {
                    id: conversation_id,
                    parties: body.hits.hits[0]['_source'].parties,
                    created_at: body.hits.hits[0]['_source'].created_at,
                    updated_at: body.hits.hits[0]['_source'].updated_at
                }
                if (messageBody.hits && messageBody.hits.hits.length > 0) {
                    let messages = []
                    for (let item of messageBody.hits.hits) {
                        const message = {
                            id: item['_id'],
                            conversation_id: conversation_id,
                            from: item['_source'].from,
                            body: item['_source'].body,
                            created_at: item['_source'].created_at,
                            updated_at: item['_source'].updated_at
                        }
                        messages.push(message)
                    }
                    response.messages = messages
                }
                ctx.status = 200
                ctx.body = {
                    data: response
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: 'Conversation not found'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

exports.sendMessage = async ctx => {
    try {
        const { from, body: messageBody } = ctx.request.body
        const { conversation_id } = ctx.params
        const validationResult = Joi.validate({ conversation_id, messageBody, from }, sendMessageSchema)
        if (validationResult.error) {
            const error = {
                status: 400,
                title: 'Validation failed',
                message: validationResult.error.message
            }
            ctx.status = error.status
            ctx.body = {
                error: error
            }
        } else {
            // check if conversation exists
            const { body: currentConversation } = await ctx.db.search({
                index: process.env.CONVERSATIONS_INDEX,
                body: {
                    query: {
                        ids: {
                            values: [conversation_id]
                        }
                    }
                }
            })
            if (currentConversation.hits && currentConversation.hits.hits.length > 0) {
                const now = new Date().toISOString()
                const { body: createdMessage } = await ctx.db.index({
                    index: process.env.MESSAGES_INDEX,
                    body: {
                        conversation_id: conversation_id,
                        from: from,
                        body: messageBody,
                        created_at: now,
                        updated_at: now
                    }
                })
                if (createdMessage && createdMessage.result === 'created') {
                    await ctx.db.indices.refresh({ index: process.env.MESSAGES_INDEX })
                    const { body: messageToSend } = await ctx.db.search({
                        index: process.env.MESSAGES_INDEX,
                        body: {
                            query: {
                                ids: { values: [createdMessage._id] }
                            },
                            sort: [
                                { created_at: { order: 'desc' } }
                            ]
                        }
                    })
                    const newMessage = {
                        id: messageToSend.hits.hits[0]['_id'],
                        conversation_id: conversation_id,
                        from: messageToSend.hits.hits[0]['_source'].from,
                        body: messageToSend.hits.hits[0]['_source'].body,
                        created_at: messageToSend.hits.hits[0]['_source'].created_at,
                        updated_at: messageToSend.hits.hits[0]['_source'].updated_at
                    }
                    ctx.status = 201
                    ctx.body = {
                        data: newMessage
                    }
                } else {
                    const error = {
                        status: 500,
                        title: 'Internal server error',
                        message: 'Can\'t index at this time'
                    }
                    ctx.status = error.status
                    ctx.body = {
                        error: error
                    }
                }
            } else {
                const error = {
                    status: 404,
                    title: 'Not found',
                    message: 'Conversation not found'
                }
                ctx.status = error.status
                ctx.body = {
                    error: error
                }
            }
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
